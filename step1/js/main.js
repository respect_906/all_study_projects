const tabsMenu = document.querySelectorAll('.tabs-title');
tabsMenu.forEach(function(clickList) {
    clickList.addEventListener('click', function() {
        const id = this.getAttribute('data-tab');
        const tabsContent = document.querySelector(`.tabs-content[data-tab="${id}"]`);
        console.log(tabsContent);
        const activeTab = document.querySelector('.tabs-title.active');
        const activeContent = document.querySelector('.tabs-content.text-active');

        activeTab.classList.remove('active'); // 1
        clickList.classList.add('active'); // 2

        activeContent.classList.remove('text-active'); // 3
        tabsContent.classList.add('text-active'); // 4
    });
});