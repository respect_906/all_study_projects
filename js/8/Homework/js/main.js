const priceIn = document.getElementById('priceIn');
const currentPr = document.getElementById('currentPrice');
const error = document.getElementById('errorIn');
console.log(priceIn);
function inputFocus(){
    this.classList.add("focusOn");
    if (!error.classList.contains("display-none"||priceIn.classList.contains("errorInput"))){
        error.classList.add("display-none");
        priceIn.classList.remove("errorInput");
    }
}
function inputBlur(){
    const inputValue = priceIn.value;
    if (inputValue <= 0) {
        priceIn.classList.add("errorInput");
        error.classList.remove("display-none");
        return;
    }
    this.classList.remove("focusOn");
    const div = document.createElement("div");
    currentPr.appendChild(div);
    div.classList.add("currentPrice");
    currentPr.classList.remove("display-none");
    const span = document.createElement("span");
    const btn = document.createElement("button");
    btn.innerHTML = "X";
    btn.classList.add("XButton");
    span.innerHTML = `Current price: ${inputValue} $`;
    priceIn.classList.add("greenInputValue");
    div.appendChild(span);
    div.appendChild(btn);
    btn.addEventListener("click", function () {
        div.remove();
        priceIn.value = ""}
        );
}
priceIn.addEventListener('focus', inputFocus);
priceIn.addEventListener('blur', inputBlur);

