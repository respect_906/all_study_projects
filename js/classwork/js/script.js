// function returnGogi () {
//     console.log(arguments);
//     console.log(Array.from(arguments));
//     return {
//         name: 'Gogi',
//         age: 58,
//         pets: null,
//         family: undefined,
//         isShurmen: true
//     }
// }
//
// returnGogi(1,2,444,[[]],{});
//
// const array = [12,23,444,221,[12,433],"string", {}];
//
// // array.forEach((el) => console.log(el));
//
// function customForEach(callbackFunction, sourceArray) {
//     for(let index = 0; index < sourceArray.length; index++) {
//         callbackFunction(sourceArray[index],index,sourceArray)
//     }
// }
//
// customForEach((el) => console.log(el), array);

// let sum= (a,b) => a+b;
// let minus= (a,b) => a-b;
// let mul= (a,b) => a*b;
// let divide= (a,b) => a/b;
//
// let calculator = (a,b,callback) => callback(a,b);
//
// console.log(calculator(150, 16, minus));

// function customMap(callbackFunction, sourceArray) {
//     debugger;
//     const resultArray = [];
//
//     for(let index = 0; index < sourceArray.length; index++) {
//         resultArray.push(callbackFunction(sourceArray[index],index,sourceArray));
//     }
//
//     return resultArray;
// }
//
// let array = [12,23,444,221,[12,433],"string", {}];
//
// array = customMap((elem, index, srcArr) => {
//     return elem*2;
// },array);

/* ЗАДАЧА - 1
* Написать функцию, которая не принимает аргументов и спрашивает пользователя что он есть на завтрак
* Считается что пользователь может ввести только одно блюдо за один раз
* По этому нужно продолжать спрашивать ДО ТЕХ ПОР ПОКА ответом от пользователя не прийдет пустой ввод.
* КАЖДОЕ БЛЮДО, которое введет пользователь нужно помещать в массив fantasticBrackfast, который
* и будет возвращаемым значением функции.
* */

// function brackfast() {
//     let dish;
//     const fantasticBrackfast = [];
//
//     while(dish!=='') {
//         dish = prompt('enter you favorit dish, please', '');
//         if (dish !== '') {
//             fantasticBrackfast.push(dish);
//         }
//     }
//     return fantasticBrackfast;
// }
// console.log(brackfast());



/* ЗАДАЧА - 2
* Написать функцию, которая будет принимать аргументом массив, в котором каждый елемент - название блюда которое пользователь ест на завтрак.
* Нужно почередно вывести в консоль каждое блюдо из этого массива. При этом удаляя это блюдо из массива.
* Таком образом после того как все блюда окажутся в консоли - массив должен остаться пустым
* Возвращаемое значение - отсутствует
* Сделать это при помощи классического цикла for И при помощи for of
* */
// const brackfast = (dishes) => {
//   //  dishes.forEach(function () {
//    //     console.log(dishes.pop());
//    // })
//     for (let i = dishes.length; i !== 0; i--){
//         console.log(dishes.pop());
//     }
// };
// brackfast(['porrige', 'burger', 'fruit', 'apple', 'eggs', 'human bone']);

/* ЗАДАЧА - 4
* Написать функцию getItemList(), которая будет получать от пользователя строку с перечисленными через запятую названиями товаров
* (они могут повторяться). После этого нужно преобразовать строку в МАССИВ С УНИКАЛЬНЫМИ ЗНАЧЕНИЯМИ
*/
const getItemList = () => {
    const str = prompt('Input the item list please');
    const items = str.split(',');
    const set = new Set(items);
    return Array.from(set);
};
console.log(getItemList());


/* ЗАДАЧА - 5
* есть некий "склад", он же исходный массив, внутри которого лежат названия товаров.
* Пользователь желает вместо определенного товара(!одного!) вставить один или несколько новых.
*
* НУЖНО написать функцию replaceItems(insteadOf, insertValue):
* где insteadOf хранит строчное значение названия товара ВМЕСТО какого именно товара он будет вставлять новые.
* ОБЯЗАТЕЛЬНО ПРОВЕРИТЬ ЕСТЬ ЛИ ТАКОЙ ТОВАР НА СКЛАДЕ, если товара нет - спросить у пользователя корректные данные.
* insertValue - список елементов которые нужно вставить - один или несколько
*
* ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ - отсутствует
* В исходном массиве(складе) ЗАМЕНИТЬ указанный товар, НА insertValue так, чтобы длинна исходного массива изменилась, т.е. каждый введенный пользователем товар добавляется отдельным товаром в уже существующий склад.
*/
let Storage = 'apple,shmapple,asus,pinaple,gogi,burito,human bones';

const replaceItems = (insteadOf, insertValue) => {
    while (Storage.indexOf(insteadOf) === -1) {
        insteadOf = prompt('enter correct item to replace')
    }
    /* 0 - преобразовать insertValue и Storage в массив
    * 1 - найти индекс insteadOf внутри Storage
    * 2 - при помощи метода массива вставить все содержимое insertValue
     * на место insteadOf внутри insteadOf*/
    Storage = Storage.split(',');
    insertValue = insertValue.split(',');

    let a = Storage.indexOf(insteadOf);

    Storage.splice(a, 1, ...insertValue);

    Storage = Storage.join(',');
};

console.log(Storage);

replaceItems('apple', 'bjvdh,bfhhjf,hfhf');

console.log(Storage);








