const path = {
    build: {
        html: 'build',
        css: 'build/css/',
        js: 'build/js/'
    },
    src: {
        html: 'src/index.html',
        scss: 'src/scss/**/*.scss',
        js: 'src/js/*.js'
    }
};
